import { InMemoryDBModule } from '@nestjs-addons/in-memory-db';
import { Module } from '@nestjs/common';
import { ApiController } from './api.controller';

@Module({
  imports: [
    InMemoryDBModule.forRoot(),
  ],
  controllers: [ApiController]
})
export class ApiModule {}
