import { Module } from '@nestjs/common';
import { FirebaseModule } from 'nestjs-firebase';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { ApiModule } from './api/api.module';
import { PublicModule } from './public/public.module';

@Module({
  imports: [
    FirebaseModule.forRoot({
      googleApplicationCredential: 'viact-alex-firebase.json'
    }),
    AuthModule,
    ApiModule,
    PublicModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
