import { InMemoryDBEntity } from "@nestjs-addons/in-memory-db";

export interface UserProfileEntity extends InMemoryDBEntity {
  uid: string;
  filename: string;
}
