import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FirebaseAdmin, InjectFirebaseAdmin } from 'nestjs-firebase';
import { HttpService } from '@nestjs/axios';
import { AuthDto } from './auth-dto';

@Injectable()
export class AuthService {

  constructor(
    @InjectFirebaseAdmin() private readonly firebase: FirebaseAdmin,
    private httpService: HttpService
  ) { }

  async login(authDto: AuthDto): Promise<any> {
    try {
      const res = await this.httpService.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword', {
        email: authDto.email,
        password: authDto.password,
        returnSecureToken: true
      }, {
        params: {
          key: 'AIzaSyBltOPrMjn2uqXCi3uGO_mSOTKITJtkFjs'
        }
      }).toPromise();
      return res.data
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  async register(authDto: AuthDto): Promise<any> {
    try {
      return await this.firebase.auth.createUser(authDto);
    } catch (e) {
      throw new HttpException(e.message, HttpStatus.BAD_REQUEST);
    }
  }

  async verifyIdToken(token: string) {
    return this.firebase.auth.verifyIdToken(token);
  }

  async getUser(uid: string) {
    return this.firebase.auth.getUser(uid);
  }
}
