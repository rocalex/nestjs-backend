import { InMemoryDBService } from '@nestjs-addons/in-memory-db';
import { Controller, Get, Param, Req, Res, Request, HttpException, HttpStatus } from '@nestjs/common';
import { UserProfileEntity } from '../entity/user-profile-entity.interface';

@Controller('public')
export class PublicController {

  constructor(private readonly userProfileService: InMemoryDBService<UserProfileEntity>) {}
  
  @Get('images/:userId')
  getAvatar(@Param('userId') userId: string, @Req() req: Request, @Res() res: any) {
    const foundProfiles = this.userProfileService.query(r => r.uid === userId);
    if (foundProfiles.length > 0) {
      const profile = foundProfiles[0];
      res.sendFile(profile.filename, {root: 'avatars'});
    } else {
      throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    }
  }
}
