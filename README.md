# viAct - Back-End Developer

Create a NestJS backend.

- Use Firebase authentication
- User In-memory database

## Install

```
$ npm install
```

## Start

```
$ npm start
```

## API document using Swagger

<a href="http://localhost:3000/swagger-ui.html">http://localhost:3000/swagger-ui.html